// Відповіді на питання:

// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Сенс прототипного успадкування в тому, що один об'єкт можна зробити прототипом іншого. При цьому якщо властивість не знайдено в об'єкті - вона береться з прототипу.
// Коли ми хочемо прочитати властивість із створеного об’єкта, а воно відсутнє, JavaScript автоматично бере його з прототипу, або прототипа прототипа і т. д. (якщо він існує).


// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// У конструкторі ключове слово super() використовується як функція, що викликає батьківський конструктор. Ключове слово super також може бути використане для виклику функцій батьківського об'єкта.


// Завдання №1
class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    getName() {
      return this._name;
    }
    getAge() {
      return this._age;
    }
    getSalary() {
      return this._salary;
    }
  
    setName(name) {
      this._name = name;
    }
    setAge(age) {
      this._age = age;
    }
    setSalary(salary) {
      this._salary = salary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
    getSalary() {
      return this._salary * 3;
    }
    getLang() {
      return this._lang;
    }
  
    setLang(lang) {
      return (this._lang = lang);
    }
  }
  
  const employee = new Employee('Viktor', 35, 5000);
  console.log(employee);
  console.log(employee.getAge());
  employee.setAge(43);
  console.log(employee.getAge());
  console.log(employee._name);
  
  const programmer = new Programmer('Denis', 34, 3000, 'c++');
  console.log(programmer);
  console.log(programmer.getAge());
  console.log(programmer.getLang());
  console.log(programmer.getSalary());
  
  const programmer2 = new Programmer('Olha', 22, 1000, 'js');
  console.log(programmer2);
  